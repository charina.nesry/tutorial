import React, { Component } from 'react';
import List from './List.js';
import './App.css';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      term: '',
      items: [],
      clicks:0,
      show:true
    };
  }

  onCange = (event) => {
    this.setState({term: event.target.value})
  }

  onSubmit = (event) => {
    event.preventDefault();
    this.setState({
      term: '',
      items: [...this.state.items, this.state.term]
    });
  }

  IncrementItem = () =>{
    this.setState({clicks: this.state.clicks +1 });
  }
  DecreaseItem = () => {
    this.setState({clicks: this.state.clicks -1 });
  }
  ToggleClick = () => {
    this.setState({show: !this.state.show});
  }

  render() {
    return (
      <div>
      <form className="App" onSubmit={this.onSubmit}>
      <input value={this.state.term} onChange={this.onCange} />
      <button>Submit</button>
      </form>
      <List items={this.state.items} />

      <button onClick={this.IncrementItem}>Click to increment by 1</button>
        <button onClick={this.DecreaseItem}>Click to decrease by 1</button>
        <button onClick={this.ToggleClick}>
          { this.state.show ? 'Hide number' : 'Show number' }
        </button>
        { this.state.show ? <h2>{ this.state.clicks }</h2> : '' }
      </div>
    );
  }
}
